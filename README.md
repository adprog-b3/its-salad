# It's Salad

It’s SALAD! adalah sebuah aplikasi berbasis website yang dapat digunakan oleh
Client untuk menerima request dari customer (auto-generated dari program)
berupa permintaan menu Salad.

### Pengembang
Terdiri dari mahasiswa/i yang tergabung dalam kelompok 3 mata kuliah _Advance Programming_ kelas B
1. Muzakki Hassan
2. Idris Yoga 
3. Eugene Brigita
4. Ilma Ainur Rohma
5. Louisa Natalika